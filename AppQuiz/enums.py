from django.utils.translation import ugettext_lazy as _

class QuizQuestionsTypes:
    TEXT = 'TEXT'
    SELECT = 'SELECT'
    IMAGE = 'IMAGE'
    VIDEO = 'VIDEO'
    SOUND = 'SOUND'
    DADOS = 'DADOS'
    CARDS = 'CARDS'



    choices = (
        (TEXT, _('Text')),
        (SELECT, _('Select')),
        (IMAGE, _('Image')),
        (VIDEO, _('Video')),
        (SOUND, _('Sound')),
        (DADOS, _('Dados')),
        (CARDS, _('Cartas')),
    )
