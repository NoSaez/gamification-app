from django.db import models
from django.utils.translation import ugettext_lazy as _
# from django.contrib.postgres.fields import JSONField

#models
from AppBase.models import BaseModel

#enums
from .enums import QuizQuestionsTypes

class BaseQuiz(BaseModel):
    class Meta:
        abstract = True

class Quiz(BaseQuiz):
    name = models.CharField(
        max_length=255,
        verbose_name=_("Name")
    )
    description = models.TextField(
        verbose_name=_("Description"),
        null=True,
        blank=True,
    )

    class Meta:
        abstract = True

    def __str__(self):
        return self.name

class QuizQuestion(BaseQuiz):
    name = models.CharField(
        max_length=255,
        verbose_name=_("Name")
    )
    text = models.TextField()
    help_text = models.TextField(null=True, blank=True)
    required = models.BooleanField(default=True)
    # quiz = models.ForeignKey(Quiz)
    question_type = models.CharField(max_length=20, choices=QuizQuestionsTypes.choices)
    points = models.IntegerField(null=True, blank=True)

    class Meta:
        abstract = True

    def __str__(self):
        return self.name

class QuizResponse(BaseQuiz):
    # quiz = models.ForeignKey(Quiz)
    # user = models.ForeignKey(
    #     "AppUser.User",
    #     verbose_name=_('User'),
    #     on_delete=models.PROTECT,
    # )
    points = models.IntegerField(null=True, blank=True)
    # start_date = models.DateTimeField(null=True, blank=True)
    # end_date = models.DateTimeField(null=True, blank=True)
    # data_answers = JSONField(verbose_name=_("Answers"), null=True, blank=True)
    data_answers = models.CharField(verbose_name=_("Answers"), max_length=250, null=True, blank=True)

    class Meta:
        abstract = True

    def __str__(self):
        quiz = getattr(self, "quiz", "")
        user = getattr(self, "user", "")
        if not quiz and not user:
            return "New Response"
        return "{} - {}".format(quiz, user)

class QuizAnswerBase(BaseQuiz):
    # question = models.ForeignKey(QuizQuestion)
    # response = models.ForeignKey(QuizResponse)
    YN_CHOICES = [
        ('yes', 'Si'),
        ('no', 'No'),
    ]

    is_correct = models.CharField(
        max_length=3,
        verbose_name=_('Is correct'),
        choices=YN_CHOICES,
        null=True,
    )
    points = models.IntegerField(null=True, blank=True)
    class Meta:
        abstract = True

    def __str__(self):
        name = getattr(self, "body", "")
        return "{}".format(name)

class QuizQuestionTextOption(BaseQuiz):
    # question = models.ForeignKey(QuizQuestion)
    value = models.CharField(max_length=50,verbose_name=_("value"))
    text = models.TextField(verbose_name=_("Text"))
    class Meta:
        abstract = True
    def __str__(self):
        return self.name

class QuizQuestionChoiceOption(QuizQuestionTextOption):
    # question = models.ForeignKey(QuizQuestion)
    is_correct = models.BooleanField()
    value = models.CharField(max_length=50,verbose_name=_("value"))
    text = models.TextField(verbose_name=_("Text"))
    class Meta:
        abstract = True
    def __str__(self):
        return self.name