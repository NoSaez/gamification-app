from locust import HttpUser, between, task
import requests
import json

class HelloWorldUser(HttpUser):
# https://cmh-back.azurewebsites.net
    @task
    def on_start(self):
        # self.client.post("/", data={'username':'', 'password':'test123'})
        response = self.client.get('/')
        csrftoken = response.cookies['csrftoken']
        self.client.post(
            '/',
            {
                'username': 'admin@gmail.com',
                'password': 'test123',
                'csrfmiddlewaretoken': csrftoken
            },
            headers={
                'X-CSRFToken': csrftoken,
        })