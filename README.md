Python 3.5.**

cryptography          1.7.1
Django                2.2.24
djangocms-admin-style 2.0.2
djangorestframework   3.12.4
idna                  2.2
keyring               10.1
keyrings.alt          1.3
pip                   20.3.4
psycopg2              2.7.7
pyasn1                0.1.9
pycrypto              2.6.1
pygobject             3.22.0
pytz                  2021.1
pyxdg                 0.25
SecretStorage         2.3.1
setuptools            50.3.2
six                   1.10.0
sqlparse              0.4.2
wheel                 0.29.0


Postgres -- name db : gamification
