from django.db import models
from django.utils.translation import ugettext_lazy as _

from AppBase.models import BaseModel
from AppBase.enums import Status

# Create your models here.
class Company(BaseModel):

    name = models.CharField(
        _('name'),
        max_length=255,
    )
    status = models.CharField(
        _('status'),
        max_length=255,
        choices=Status.choices,
        default=Status.ENABLE,
    )

    users_number = models.IntegerField(null=True, blank=True)

    class Meta:
        verbose_name = _('Company')
        verbose_name_plural = _('Companys')

    def __str__(self):
        return '{}'.format(self.name)

class CompanyCampaingAssignment(BaseModel):
    poll_campaign = models.ForeignKey(
        'AppPoll.PollCampaign',
        verbose_name=_('poll campaing'),
        on_delete=models.CASCADE,
    )
    company = models.ForeignKey(
        Company,
        verbose_name=_('Company'),
        on_delete=models.CASCADE,
    )
    class Meta:
        verbose_name = _('Company Campaign Assignment')
        verbose_name_plural = _('Company Campaign Assignment')
        unique_together = ('poll_campaign', 'company')