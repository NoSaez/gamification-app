from django.shortcuts import render
from django.http import HttpResponse
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.http import HttpResponse, HttpResponseRedirect

from AppPoll.models import PollQuestion
from AppPoll.models import Poll
from AppPoll.models import QuestionResponse

from AppBase.secure import SecureTemplateView
# Create your views here.
import json


class BaseTemplateView(SecureTemplateView):
    def get_context_data(self, **kwargs):
        context = super(BaseTemplateView, self).get_context_data(**kwargs)

        if hasattr(self, 'title'):
            context['title'] = self.title

        return context

class QuizResponse(BaseTemplateView):
    from django.views.decorators.csrf import csrf_exempt

    @csrf_exempt
    def post(request, pk):
        from datetime import datetime
        post_file = dict(request.FILES.lists())
        post_data = dict(request.POST.lists())
        for item in post_file:
            try:
                del post_data['question_'+item[9:]]
            except:
                print("no hay :D")
        del post_data['csrfmiddlewaretoken']
        for item in post_file:
            try:
                response_id = post_data[item][2]
            except:
                response_id = None

            if response_id:
                if post_data[item][1] == '':
                    post_data[item][1] = 0
                QuestionResponse.objects.filter(id = response_id).first().update(points = post_data[item][1])
            else:
                question_id = item[9:]
                type_question = PollQuestion.objects.values_list('question_type',flat=True).filter(id = question_id).first()
                if type_question == 'IMAGE' or type_question == 'VIDEO':
                    image_id = 'question_'+question_id
                    file = request.FILES.get(image_id)
                    fs = FileSystemStorage(location='AppPoll/static/question_images')
                    filename = fs.save(file.name, file)
                    uploaded_file_url = fs.url(filename)
                    post_file[image_id] = uploaded_file_url

                    new_response = QuestionResponse()
                    new_response.question = PollQuestion.objects.filter(id = question_id).first()
                    new_response.user = request.user
                    new_response.poll = Poll.objects.filter(id = pk).first()
                    new_response.data_answers = post_file[item]
                    new_response.save()

        for item in post_data:
            try:
                response_id = post_data[item][2]
            except:
                response_id = None
            if response_id != 'null' and response_id != None:
                if post_data[item][1] == '':
                    post_data[item][1] = 0
                QuestionResponse.objects.filter(id = response_id).first().update(points = post_data[item][1])
            else:
                question_id = item[9:]
                image_id = 'question_'+question_id
                new_response = QuestionResponse()
                new_response.question = PollQuestion.objects.filter(id = question_id).first()
                new_response.user = request.user
                new_response.poll = Poll.objects.filter(id = pk).first()
                new_response.points = 0
                new_response.data_answers = post_data[item][0]
                new_response.save()


        return  HttpResponseRedirect('/home/')