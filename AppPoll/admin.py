# -*- coding: utf-8 -*-
""" Administration classes for the polls application. """
# standard library

# django
from django.contrib import admin
from django.urls import reverse

from django import forms
# from selectable.forms import AutoCompleteSelectMultipleWidget, AutoComboboxSelectWidget

# models
from .models import Poll
from .models import PollQuestion
from .models import PollCampaign
from .models import PollAssignment
from .models import QuestionResponse
from .models import QuestionTextOption
from .models import PollQuestionChoiceOption


# from adminsortable2.admin import SortableInlineAdminMixin

# other
# from import_export import resources
# from import_export.admin import ImportExportActionModelAdmin
# from django.utils.translation   import ugettext_lazy as _
# from users.lookups import UserLookup
# from clients.lookups import ClientLookup

class PollCampaignInline(admin.StackedInline):
    model = PollCampaign
    extra = 1
    fk_name = "quiz"
    fields = ('name', 'quiz', 'status',)

class PollQuestionInline(admin.TabularInline):
    model = PollQuestion
    extra = 0
    fk_name = "quiz"
    fields = ('name', 'text', 'help_text','question_type', 'required')

    # fields = ('name', 'text', 'help_text','question_type', 'required', 'changeform_link')
    # readonly_fields = ['changeform_link']

    # def changeform_link(self, instance):
    #         info = ('polls', 'pollquestion')
    #         link = reverse("admin:%s_%s_%s" % (info + ('change',)),
    #                     current_app=self.admin_site.name, args=('__fk__',))
    #         if instance.id:
    #             changeform_url = reverse(
    #                 'admin:polls_pollquestion_change', args=(instance.id,)
    #             )
    #             return '<a id="change_id_pollquestion_set_{id}" class="related-widget-wrapper-link" data-href-template="{}" href="{}?_to_field=id&_popup=1">{}</a>'.format(link, changeform_url, _("Detail"), id=instance.id)
    #         return ""
    # changeform_link.allow_tags = True
    # changeform_link.short_description = ''

@admin.register(Poll)
class PollAdmin(admin.ModelAdmin):
    list_display = ('name', 'description')
    inlines = [PollQuestionInline, PollCampaignInline]

@admin.register(PollCampaign)
class PollCampaignAdmin(admin.ModelAdmin):
    list_display = ('name','status', 'all_clients')


class PollAssignmentAdminForm(forms.ModelForm):
    class Meta(object):
        model = PollAssignment
        fields = '__all__'
        # widgets = {
        #     'client': AutoComboboxSelectWidget(lookup_class=ClientLookup),
        # }

@admin.register(PollAssignment)
class PollAssignmentAdmin(admin.ModelAdmin):
    list_display = ('poll_campaign', 'poll', 'user')
    fk_name = "poll_campaign"

@admin.register(QuestionResponse)
class QuestionResponseAdmin(admin.ModelAdmin):
    list_display = ('question', 'user', 'poll', 'points', 'data_answers')

@admin.register(QuestionTextOption)
class QuestionTextOptionAdmin(admin.ModelAdmin):
    list_display = ('question', 'text', 'value')

@admin.register(PollQuestionChoiceOption)
class PollQuestionChoiceOptionAdmin(admin.ModelAdmin):
    list_display = ('question', 'is_correct','text','value')

@admin.register(PollQuestion)
class PollQuestionChoiceOptionAdmin(admin.ModelAdmin):
    list_display = ('name', 'text', 'help_text','question_type', 'required')