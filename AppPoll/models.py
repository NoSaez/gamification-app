from django.db import models
from django.utils.translation import ugettext_lazy as _

#models
from AppBase.models import BaseModel
from AppQuiz.models import Quiz
from AppQuiz.models import QuizQuestion
from AppQuiz.models import QuizResponse
from AppQuiz.models import QuizQuestionTextOption
from AppQuiz.models import QuizQuestionChoiceOption
from AppUser.models import User

#enums
from .enums import PollStatus
from AppQuiz.enums import QuizQuestionsTypes

class Poll(Quiz):
    class Meta:
        verbose_name = _('Poll')
        verbose_name_plural = _('Polls')

class PollCampaign(BaseModel):

    all_clients = models.BooleanField(verbose_name=_('All clients'), default=False)

    # max_clients = models.IntegerField(
    #     verbose_name=_('Answers required'),
    #     help_text=_("Amount of required answered. After the user response this amount of quizzes, no more quizzes will be requiered."),
    #     blank=True,
    #     null=True,
    # )

    # min_answers = models.IntegerField(
    #     verbose_name=_('Min answers'),
    #     help_text=_("Minimum questions that must be answered"),
    #     blank=True,
    #     null=True,
    #     default=0,
    # )

    all_client_company = models.ManyToManyField(
        "AppCompany.Company",
        verbose_name=_('Assessed company'),
        blank=True,
    )

    # foreign keys
    name = models.CharField(
        max_length=100,
        verbose_name=_('Poll Campaign'),
        null=True,
        blank=True,
    )
    status = models.PositiveSmallIntegerField(
        choices = PollStatus.choices,
        default = PollStatus.DRAFT,
    )

    start_date = models.DateField(
        verbose_name=_("Start date"),
        null=True,
        blank=True,
    )
    end_date = models.DateField(
        verbose_name=_("End date"),
        null=True,
        blank=True,
    )
    quiz = models.ForeignKey(
        Poll,
        verbose_name=_('Client poll'),
        on_delete=models.CASCADE,
    )

    class Meta:
        verbose_name = _('Poll Campaign')
        verbose_name_plural = _('Polls Campaigns')


    def __str__(self):
        return "{} - {}".format(self.quiz, self.name)

    # def get_response_url(self, code=None):
    #     response_url = "/encuestas/client-poll-test/{}/{}/".format(self.quiz.id, self.id)
    #     if self.status == ClientPollStatus.TEST and code:
    #         response_url = "/encuestas/client-poll/{}/{}/{}/".format(self.quiz.id, self.id, code)
    #     return response_url

class PollQuestion(QuizQuestion):
    quiz = models.ForeignKey(Poll, on_delete=models.PROTECT, null=True, blank=True)
    question_type = models.CharField(max_length=20, choices=QuizQuestionsTypes.choices)
    class Meta:
        verbose_name = _('Poll Question')
        verbose_name_plural = _('Polls Questions')

class QuestionResponse(QuizResponse):
    question = models.ForeignKey(PollQuestion, on_delete=models.PROTECT)
    user = models.ForeignKey(
        User,
        verbose_name=_('User'),
        on_delete=models.PROTECT,
        null=True,
        blank=True,
    )
    poll = models.ForeignKey(
       Poll,
        verbose_name=_('poll campaign'),
        on_delete=models.PROTECT,
    )

    # points = models.IntegerField(null=True, blank=True)

    # data_answers = models.CharField(verbose_name=_("Answers"), max_length=250, null=True, blank=True)

    class Meta:
        verbose_name = _('Poll Question Response')
        verbose_name_plural = _('Polls Questions Responses')
        unique_together = ('question', 'user', 'poll')

class PollAssignment(BaseModel):
    poll_campaign = models.ForeignKey(
        PollCampaign,
        verbose_name=_('User Poll'),
        on_delete=models.CASCADE,
    )
    poll = models.ForeignKey(
        Poll,
        verbose_name=_('User Poll'),
        on_delete=models.CASCADE,
    )
    user = models.ForeignKey(
        User,
        verbose_name=_('User'),
        on_delete=models.CASCADE,
    )

    def get_response_url(self):
        return "/encuestas/poll/{}/{}/{}/".format(self.poll.id, self.poll_campaign.id, self.user.email)

    class Meta:
        verbose_name = _('Poll Assignment')
        verbose_name_plural = _('Polls Assignment')
        unique_together = ('poll_campaign', 'user', 'poll')

class QuestionTextOption(QuizQuestionTextOption):
    question = models.ForeignKey(PollQuestion, on_delete=models.CASCADE)
    class Meta:
        verbose_name = _('poll question Text')
        verbose_name_plural = _('poll questions Text')

class PollQuestionChoiceOption(QuizQuestionChoiceOption):
    question = models.ForeignKey(PollQuestion, on_delete=models.CASCADE)
    class Meta:
        verbose_name = _('poll question choice')
        verbose_name_plural = _('poll questions choices')



#########################################
#Modelo campains y poll
class CampaingyPoll(models.Model):
    poll = models.ForeignKey(Poll, on_delete=models.CASCADE)
    campaing= models.ForeignKey(PollCampaign,on_delete=models.CASCADE)

