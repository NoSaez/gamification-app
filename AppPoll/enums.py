from django.utils.translation   import ugettext_lazy as _

class PollStatus(object):
    DRAFT = 0
    ACTIVE = 1
    CLOSED = 2
    # ARCHIVED = 3
    # TEST = 4

    choices = [
        (DRAFT, _('DESACTIVADO')),
        (ACTIVE, _('ACTIVADO')),
        (CLOSED, _('CERRADO')),
        # (ARCHIVED, _('Archived')),
        # (TEST, _('Test'))
    ]
