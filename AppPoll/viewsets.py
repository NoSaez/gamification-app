from Api.viewsets import ModelViewSet
from rest_framework import permissions
from rest_framework.response import Response
from .serializers import PollAssignmentSerializer

from .models import PollAssignment
from .models import QuestionResponse

class MyPollsViewsets(ModelViewSet):
    model = PollAssignment
    serializer_class = PollAssignmentSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context.update({'user': self.request.user})
        return context

    def get_queryset(self):
        if self.request.user.role =='AS':
            qs = super().get_queryset().filter(user = self.request.user)
            print(qs)
        else:
            poll_assigment = PollAssignment.objects.filter(user=self.request.user).values('poll') # question que son asignadas a mi
            print(poll_assigment)
            user_assigment = QuestionResponse.objects.filter(poll__in = poll_assigment).exclude(user = self.request.user).values('user')
            resp_assigment = QuestionResponse.objects.filter(poll__in = poll_assigment).exclude(user = self.request.user).values('poll')

            print(user_assigment)
            id_assigment = PollAssignment.objects.filter(poll__in= resp_assigment , user__in = user_assigment).values('id')
            print(id_assigment)
            qs = super().get_queryset().filter(id__in= id_assigment)

        return qs