from django.contrib import admin
from django.urls import path, include
from .views import QuizResponse

urlpatterns = [
    path('quiz_response/<int:pk>/', QuizResponse.post, name='quizresponse'),

]
