from django.db.models.query_utils import select_related_descend

from .models import PollAssignment
from .models import Poll
from .models import PollQuestion
from .models import PollQuestionChoiceOption
from .models import PollCampaign
from .models import QuestionResponse

from rest_framework import serializers

class PollSerializer(serializers.ModelSerializer):
    class Meta:
        model = Poll
        fields = '__all__'

    def to_representation(self, instance):
        result = super().to_representation(instance)
        result['question'] = PollQuestion.objects.filter(quiz = result['id']).values()
        for item in result['question']:
            if item['question_type'] == "SELECT":
                item['options'] = PollQuestionChoiceOption.objects.filter(question_id = item['id']).values()
        return result


class PollCampaignSerializer(serializers.ModelSerializer):
    class Meta:
        model = PollCampaign
        fields = '__all__'

class PollAssignmentSerializer(serializers.ModelSerializer):
    poll = PollSerializer()
    poll_campaign = PollCampaignSerializer()
    class Meta:
        model = PollAssignment
        fields = '__all__'

    def to_representation(self, instance):
        result = super().to_representation(instance)
        quiz_responses = QuestionResponse.objects.values_list('poll',flat=True).filter(user = instance.user, poll = result['poll']['id']).values()
        if self.context['user'].role == 'CO':
            result['evaluate'] = True
        else:
            result['evaluate'] = False
        result['responses'] = quiz_responses
        return result