from django.utils.translation import ugettext_lazy as _

class Status(object):
    ENABLE = 'EN'
    DISABLE = 'DI'

    choices = (
        (ENABLE, _('HABILITADO')),
        (DISABLE, _('DESABILITADO')),
    )