from django.utils import timezone

def clean_query_string(request):
    clean_query_set = request.GET.copy()

    clean_query_set = dict(
        (k, v) for k, v in request.GET.items() if k != 'o'
    )

    try:
        del clean_query_set['p']
    except:
        pass

    mstring = []
    for key in clean_query_set.keys():
        valuelist = request.GET.getlist(key)
        mstring.extend(['%s=%s' % (key, val) for val in valuelist])

    return '&'.join(mstring)


def today():
    """
    This method obtains today's date in local time
    """
    return timezone.localtime(timezone.now()).date()