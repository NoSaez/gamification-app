#from _typeshed import Self
from typing import Type
from django.core.exceptions import ImproperlyConfigured
from django.shortcuts import render#, render_to_response
from django.contrib.auth import views as auth_views
from django.views.generic import TemplateView , ListView
from django.utils.translation import ugettext_lazy as _
from django.shortcuts import  render, redirect
from django.contrib.auth import login
from django.contrib import messages

from .secure import SecureTemplateView, BaseListView, BaseDetailView

from .utils import today
from django.contrib.auth import login, authenticate
from django.contrib.auth.decorators import login_required, permission_required

from AppPoll.models import PollAssignment , CampaingyPoll,Poll, PollCampaign
from AppPoll.models import QuestionResponse , QuestionTextOption,PollQuestionChoiceOption

from django.db.models   import Q

from .forms import NewPollCampaignsForm, NewPollForm, NewUserForm , CompanyForm,NewPollQuestionsForm,FormQuestionsText,FormQuestionsChoice, formemail

from AppUser.models import User
from AppCompany.models import Company
from AppQuiz.models import Quiz
from django.http import HttpResponseRedirect, request
from django.views.generic import  CreateView
from django.urls import reverse_lazy
#from _typeshed import Self
from AppUser.enums import Type

#Main inicio normal
class DashboardView(SecureTemplateView):
    title = "Bienvenido"
    template_name = 'base/main.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        if self.request.user.role == Type.CONSULTANT:
            quiz_responses = QuestionResponse.objects.values_list('poll',flat=True).filter(user = self.request.user)
            context['poll_active'] = PollAssignment.objects.filter(Q(user = self.request.user) & Q(Q(poll_campaign__end_date__gte = today()) | Q(poll_campaign__end_date = None)) ).exclude(poll__in = quiz_responses).count()
            context['poll_end'] = PollAssignment.objects.filter(Q(user = self.request.user) & Q(poll__in = quiz_responses)).count()
            context['poll_all'] = PollAssignment.objects.filter(user = self.request.user).count()

        else:
            quiz_responses = QuestionResponse.objects.values_list('poll',flat=True).filter(user = self.request.user)
            context['poll_active'] = PollAssignment.objects.filter(Q(user = self.request.user) & Q(Q(poll_campaign__end_date__gte = today()) | Q(poll_campaign__end_date = None)) ).exclude(poll__in = quiz_responses).count()
            context['poll_end'] = PollAssignment.objects.filter(Q(user = self.request.user) & Q(poll__in = quiz_responses)).count()
            context['poll_all'] = PollAssignment.objects.filter(user = self.request.user).count()

        context['role'] = self.request.user.role
        return context

    def get(self,request):
        if request.user.role == Type.ADMINISTRATOR:
            self.template_name = 'base/dashboard_admin.html'
        elif request.user.role == Type.ASSESSED:
            self.template_name = 'base/main.html'
        elif request.user.role == Type.CONSULTANT:
            self.template_name = 'base/main.html'

        return super(DashboardView,self).get(request)


#administrator
@login_required
def viewAdmin (request):

    return render(request,"base/dashboard_admin.html")


#crear usuario
class AdminDashboardView(SecureTemplateView):
    title = "Bienvenido"
    template_name = 'views/new_user.html'

    def get_context_data(self, **kwargs):
        form = NewUserForm()
        context={"register_form":form}
        return context

    def post(self, *args, **kwargs):

        if self.request.method == "POST":
            form = NewUserForm(self.request.POST)
            if form.is_valid():
                user = form.save()
                #login(self.request, user)
                messages.success(self.request, "Registration successful." )
                #return HttpResponseRedirect(self.request, 'base/dashboard_admin.html')
                #return render(self.request, 'base/dashboard_admin.html')
            messages.error(self.request, "Unsuccessful registration. Invalid information.")
            context = {'users': User.objects.all()}
            return render(self.request, "views/list_user.html",context)

# User views
class LoginView(auth_views.LoginView):
    title = 'Login Gamificación'
    template_name = 'registration/login.html'

class PasswordChangeView(TemplateView):
    """ view that renders the password change form """
    template_name = "registration/password_change_form.pug"

class PasswordResetView(auth_views.PasswordResetView):
    template_name = 'registration/password_reset_form.html'

class PasswordResetDoneView(auth_views.PasswordResetDoneView):
    template_name = "registration/password_reset_done.html"

class PasswordResetConfirmView(auth_views.PasswordResetConfirmView):
    template_name = "registration/password_reset_confirm.html"

class PasswordResetCompleteView(auth_views.PasswordResetCompleteView):
    template_name = "registration/password_reset_complete.html"

#TODO configure
class GameUserChangePassView(auth_views.PasswordChangeView):
    title = _("Profile")
    template_name = 'registration/change-password.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['can_save'] = False
        context['menu_item'] = "change-password"
        return context

class GameUserChangePassDoneView(TemplateView):
    title = _("Profile")
    template_name = 'registration/change-password-done.html'


####
#Listar usuarios
@login_required
def list_user(request):
    users = User.objects.all()
    data= {
        'users':users
    }
    return render(request,'views/list_user.html',data)

##Nueva empresa
class new_company(SecureTemplateView):
    #title = "Bienvenido"
    template_name = 'views/new_company.html'

    def get_context_data(self, **kwargs):
        form = CompanyForm()
        context={"form":form}
        return context

    def post(self, *args, **kwargs):

        print("#############")
        if self.request.method == "POST":
            form = CompanyForm(self.request.POST)
            if form.is_valid():
                sv_company = form.save()
                #login(self.request, poll)
                messages.success(self.request, "Creation successful." )
                #return HttpResponseRedirect("")
            messages.error(self.request, "Unsuccessful registration. Invalid information.")
            context = {'companies': Company.objects.all()}
            return render(self.request, "views/list_company.html",context)



#Listar Empresas
@login_required
def list_company(request):
    companies = Company.objects.all()
    data= {
        'companies':companies
    }
    return render(request,'views/list_company.html',data)


##New Poll
class NewPollView(SecureTemplateView):
    #title = "Bienvenido"
    template_name = 'views/new_poll.html'

    def get_context_data(self, **kwargs):
        form = NewPollForm()
        context={"poll_form":form}
        return context

    def post(self, *args, **kwargs):

        print("#############")
        if self.request.method == "POST":
            form = NewPollForm(self.request.POST)
            if form.is_valid():
                poll = form.save()
                #login(self.request, poll)
                messages.success(self.request, "Creation successful." )
                #return HttpResponseRedirect("")
            messages.error(self.request, "Unsuccessful registration. Invalid information.")
            context = {'object_list': Poll.objects.all()}
            return render(self.request, "views/list_poll.html", context)


##Listar Poll

from AppPoll.models import PollQuestion,PollCampaign,PollQuestion

class PollList(SecureTemplateView):
    title = "Actividades"
    template_name = 'views/list_poll.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object_list'] = Poll.objects.all()
        return context


##New PollCampaign

class NewPollCampaignView(SecureTemplateView):
    #title = "Bienvenido"
    template_name = 'views/new_PollCampaign.html'
    def get_context_data(self, **kwargs):
        form = NewPollCampaignsForm()
        context={"poll_form":form}
        return context

    def post(self, *args, **kwargs):
        print("#############")
        if self.request.method == "POST":
            form = NewPollCampaignsForm(self.request.POST)
            if form.is_valid():
                pollcam = form.save()
                #login(self.request, pollcam)
                messages.success(self.request, "Creation successful." )
                #return HttpResponseRedirect("")
            messages.error(self.request, "Unsuccessful registration. Invalid information.")
            context = {'object_list':PollCampaign.objects.all()}
            return render(self.request, "views/list_pollCampaing.html", context)



##listar compañia

class PollListCampaing(SecureTemplateView):
    title = "Actividades"
    template_name = 'views/list_pollcampaing.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object_list'] = PollCampaign.objects.all()
        return context




##Polls Questions

class NewPollQuestionsView(SecureTemplateView):
    #title = "Bienvenido"
    template_name = 'views/new_PollQuestions.html'

    def get_context_data(self, **kwargs):
        form = NewPollQuestionsForm()
        context={"poll_form":form}
        return context

    def post(self, *args, **kwargs):

        if self.request.method == "POST":
            form = NewPollQuestionsForm(self.request.POST)
            if form.is_valid():
                poll = form.save()
                #login(self.request, poll)
                messages.success(self.request, "Creation successful." )
               #return HttpResponseRedirect("")
            context = {'object_list':PollQuestion.objects.all()}
            return render(self.request, "views/list_PollQuestions.html", context)

##listar pregunta

class PollListQuestions(SecureTemplateView):
    title = "Actividades"
    template_name = 'views/list_PollQuestions.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object_list'] = PollQuestion.objects.all()
        return context

####################################

class PollAsignamentView(SecureTemplateView):
    template_name = 'base/Assigments.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        from AppPoll.models import PollCampaign
        context['list_company'] = Company.objects.all().values()
        context['list_user'] = User.objects.filter()
        context['list_campaign'] = PollCampaign.objects.all()

        return context

    def post(self, *args, **kwargs):
        id_company = self.request.POST.get('selectEmpresa', None)
        campaign = PollCampaign.objects.filter(id = self.request.POST.get('selectCampana')).first()
        user = User.objects.filter(id = self.request.POST.get('selectUsuario')).first()

        data = {'poll_campaign': campaign,
        'poll':campaign.quiz,
        'user': user}

        if id_company:
            company = Company.objects.filter(id = id_company).first()
            users_company = User.objects.filter(company = company)
            print(users_company)
        for item in users_company:
            if not PollAssignment.objects.filter(poll_campaign = campaign.id, poll = campaign.quiz, user = user.id):
                data_user_company = {'poll_campaign': campaign,
                    'poll':campaign.quiz,
                    'user': item}
            new_asignament = PollAssignment(**data_user_company)
            new_asignament.save()
            messages.success(self.request, "Asignacion Creada." )

        if PollAssignment.objects.filter(poll_campaign = campaign.id, poll = campaign.quiz, user = user.id):
            messages.error(self.request, "Usuario ya esta asignado a esta campaña.")
            return HttpResponseRedirect("/new_asignament")
        else:
            new_asignament = PollAssignment(**data)
            new_asignament.save()
            messages.success(self.request, "Asignacion Creada." )
            return HttpResponseRedirect("/new_asignament")


class PollAsignamentListView(SecureTemplateView):
    template_name = 'base/list_assigments.html'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['list'] = PollAssignment.objects.all()
        return context
####################################
##Campaing and poll

class CreatePollView(CreateView):
        model = Poll
        template_name = 'views/new_pollFull.html'
        form_class = NewPollForm
        second_form_class =NewPollQuestionsForm
        tree_form_class= NewPollCampaignsForm
        success_url = reverse_lazy('new_pollFull')

        def get_context_data(self, **kwargs):
            context = super(CreatePollView, self).get_context_data(**kwargs)
            if 'form' not in context:
                context['form'] = self.form_class(self.request.GET)
            if 'form2' not in context:
                context['form2'] = self.second_form_class(self.request.GET)
            if 'form3' not in context:
                context['form3'] = self.tree_form_class(self.request.GET)
            return context

        def post(self, request, *args, **kwargs):
            self.object = self.get_object
            form = self.form_class(request.POST)
            form2 = self.second_form_class(request.POST)
            form3=self.tree_form_class(request.POST)
            if form.is_valid() and form2.is_valid():
                solicitud = form.save()
                solicitud = form2.save()
                solicitud = form3.save()
                solicitud.save()
                return HttpResponseRedirect(self.get_success_url())
            else:
                return self.render_to_response(self.get_context_data(form=form, form2=form2))


################3
#Template Poll
@login_required
def viewpoll(request):
    return render(request,'base/new_pollCompleto.html')


###################3
#views Games-Dado

class Game_dado(SecureTemplateView):
    template_name = 'games/dado.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object_list'] = Poll.objects.all()
        return context

#views Games-carta

class Game_card(SecureTemplateView):
    template_name = 'games/carta.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object_list'] = Poll.objects.all()
        return context



#######################
from AppCompany.models import Company
#from django.shortcuts import render , redirect
@login_required
def editar_company(request ,id):
    company= Company.objects.get(id=id)
    data ={
        'form':CompanyForm(instance=company)
    }
    if request.method  == 'POST':
        form = CompanyForm(data=request.POST,instance=company)
        if form.is_valid():
            form.save()
            data['mensaje']='Editado correctamente'
            data['form']=form
            return redirect(to='list_company')

    return render(request, 'views/edit_company.html',data)




from AppUser.models import User
from django.contrib.auth import get_user_model
#from django.shortcuts import render , redirect
@login_required
def editar_user(request ,id):
    user= User.objects.get(id=id)
    data ={
        'form':NewUserForm(instance=user)
    }
    if request.method  == 'POST':
        form = NewUserForm(data=request.POST,instance=user)
        if form.is_valid():
            form.save()
            data['mensaje']='Editado correctamente'
            data['form']=form
            return redirect(to='list_user')

    return render(request, 'views/edit_user.html',data)



from AppPoll.models import Poll,PollCampaign,PollQuestion
#from django.shortcuts import render , redirect
@login_required
def editar_poll(request ,id):
    poll= Poll.objects.get(id=id)
    data ={
        'form':NewPollForm(instance=poll)
    }
    if request.method  == 'POST':
        form = NewPollForm(data=request.POST,instance=poll)
        if form.is_valid():
            form.save()
            data['mensaje']='Editado correctamente'
            data['form']=form
            return redirect(to='list_poll')

    return render(request, 'views/edit_poll.html',data)


@login_required
def editar_campaing(request ,id):
    campaing= PollCampaign.objects.get(id=id)
    data ={
        'form':NewPollCampaignsForm(instance=campaing)
    }
    if request.method  == 'POST':
        form = NewPollCampaignsForm(data=request.POST,instance=campaing)
        if form.is_valid():
            form.save()
            data['mensaje']='Editado correctamente'
            data['form']=form
            return redirect(to='list_pollCampaing')
    return render(request, 'views/edit_campaing.html',data)

@login_required
def editar_questions(request ,id):
    questions= PollQuestion.objects.get(id=id)
    data ={
        'form':NewPollQuestionsForm(instance=questions)
    }
    if request.method  == 'POST':
        form = NewPollQuestionsForm(data=request.POST,instance=questions)
        if form.is_valid():
            form.save()
            data['mensaje']='Editado correctamente'
            data['form']=form
            return redirect(to='list_pollQuestions')

    return render(request, 'views/edit_questions.html',data)


############################3
#Crear questions text

class NewQuestionText(SecureTemplateView):
    #title = "Bienvenido"
    template_name = 'questions/type_text.html'

    def get_context_data(self, **kwargs):
            form = FormQuestionsText()
            context={"questions_form":form}
            return context

    def post(self, *args, **kwargs):

        print("#############")
        if self.request.method == "POST":
            form = FormQuestionsText(self.request.POST)
            if form.is_valid():
                text = form.save()
                #login(self.request, poll)
                messages.success(self.request, "Creation successful." )
                return HttpResponseRedirect("")
            messages.error(self.request, "Unsuccessful registration. Invalid information.")



#Crear questions choice

class NewQuestionChoice(SecureTemplateView):
    #title = "Bienvenido"
    template_name = 'questions/type_text.html'

    def get_context_data(self, **kwargs):
            form = FormQuestionsChoice()
            context={"questions_form":form}
            return context

    def post(self, *args, **kwargs):
        context = {'object_list':PollQuestionChoiceOption.objects.all()}
        if self.request.method == "POST":
            form = FormQuestionsChoice(self.request.POST)
            if form.is_valid():
                choice = form.save()
                #login(self.request, poll)
                messages.success(self.request, "Creation successful.")
                return render(self.request, "questions/list_choice.html", context)

            messages.error(self.request, "Unsuccessful registration. Invalid information.")
            return render(self.request, "questions/list_choice.html", context)




#Listar questions text

class ListQuestionText(SecureTemplateView):
    title = "Actividades"
    template_name = 'questions/list_text.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object_list'] = QuestionTextOption.objects.all()
        return context


 #Listar questions text

class ListQuestionChoice(SecureTemplateView):
    title = "Actividades"
    template_name = 'questions/list_choice.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['object_list'] = PollQuestionChoiceOption.objects.all()
        return context


#editar pregunta tipo texto
@login_required
def editar_text(request ,id):
    texto= QuestionTextOption.objects.get(id=id)
    data ={
        'form':FormQuestionsText(instance=texto)
    }
    if request.method  == 'POST':
        form = FormQuestionsChoice(data=request.POST,instance=texto)
        if form.is_valid():
            form.save()
            data['mensaje']='Editado correctamente'
            data['form']=form

    return render(request, 'questions/edit_text.html',data)


#editar pregunta tipo choice
@login_required
def editar_choice(request ,id):
    texto= PollQuestionChoiceOption.objects.get(id=id)
    data ={
        'form':FormQuestionsChoice(instance=texto)
    }
    if request.method  == 'POST':
        form = FormQuestionsChoice(data=request.POST,instance=texto)
        if form.is_valid():
            form.save()
            data['mensaje']='Editado correctamente'
            data['form']=form
            return redirect(to='list_choice')

    return render(request, 'questions/edit_choice.html',data)

#############################################33333
#Envio emil.
from django.core.mail import EmailMessage
from django.template import RequestContext, context
def send_email(request):
    if request.method == 'POST':
        asunto='Invitacion a contestar Encuesta'
        metodo= request.POST.get("sendEmail")
        messege = """
                        Ingrese a este link para iniciar sesion en la plataforma de Thinkgo.
                        http://127.0.0.1:8000/home/ \
                        """
        mail= EmailMessage(asunto,messege,metodo,to=['ontour.concepcion@gmail.com'])
        mail.send()
        return render(request, 'base/list_assigments.html')


#########################################333
#listar procesos por empresas
#Template Poll
@login_required
def viewprocexcompa(request):
    companies = Company.objects.all()
    data= {
        'companies':companies
    }
    #return render(request,'views/list_company.html',data)
    return render(request,'views/list_procexcompa.html',data)



# @login_required

def list_procesosxempresa(request,id):
    company= Company.objects.filter(id=id).first()
    users= User.objects.filter(company=company)

    asignacion=PollAssignment.objects.filter(user__in=users).values('id','user__email','poll__name')

    context={'listcompany':asignacion}
    #return HttpResponseRedirect(request,'views/ver_procexcompa.html',context)
    return render(request,'views/ver_procexcompa.html' ,context)


def ver_respuestas(request, id):
    from itertools import chain

    target_assign_poll = PollAssignment.objects.values_list('poll__id',flat=True).filter(id=id).first()
    target_assign_user = PollAssignment.objects.values_list('user__id',flat=True).filter(id=id).first()
    message = ''
    target_resp = QuestionResponse.objects.filter(user__id =target_assign_user, poll__id = target_assign_poll).values('question__text','points','data_answers')
    if not target_resp.exists():
        message = "No hay respuestas para mostrar"
    context = {"resp":target_resp, "message":message}

    return render(request,'views/ver_respuestas.html' ,context)



def viewrespxuser(request):
    usuario = User.objects.all()
    data= {
        'users':usuario
    }
    #return render(request,'views/list_company.html',data)
    return render(request,'views/list_respxusuario.html',data)
####################################3
