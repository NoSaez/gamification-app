document.addEventListener('DOMContentLoaded', function () {
  var modeSwitch = document.querySelector('.mode-switch');

  modeSwitch.addEventListener('click', function () {
    document.documentElement.classList.toggle('dark');
    modeSwitch.classList.toggle('active');
  });

  var listView = document.querySelector('.list-view');
  var gridView = document.querySelector('.grid-view');
  var projectsList = document.querySelector('.project-boxes');
  var Level1 = document.querySelector('.project-box-wrapper');

  listView.addEventListener('click', function () {
    gridView.classList.remove('active');
    listView.classList.add('active');
    projectsList.classList.remove('jsGridView');
    projectsList.classList.add('jsListView');
    projectsList.classList.remove('row');
    Level1.classList.remove('col-12');
    Level1.classList.remove('row');

  });

  gridView.addEventListener('click', function () {
    gridView.classList.add('active');
    listView.classList.remove('active');
    projectsList.classList.remove('jsListView');
    projectsList.classList.add('jsGridView');
    projectsList.classList.add('row');
    Level1.classList.add('col-12');
    Level1.classList.add('row');

  });

  // document.querySelector('.messages-btn').addEventListener('click', function () {
  //   document.querySelector('.messages-section').classList.add('show');
  // });

  // document.querySelector('.messages-close').addEventListener('click', function () {
  //   document.querySelector('.messages-section').classList.remove('show');
  // });
});

function lanzar() {
  var dado = document.getElementById('dado');
  var numero = Math.floor((Math.random() * 6) + 1);

  dado.innerHTML = numero;
  var ndado = document.getElementById('ndado');
  ndado.value = numero;
}

function createQuiz(quiz) {
  $('label[for=choice-1]').appendTo(".card");
  $(document).ready(function () {
    $('label[for=choice-1]').click(function () {
      document.getElementById('ncard').value = "2C"
      const links = document.querySelectorAll(`section.background-select .card .color-choices div label`);
      links[0].className += " checkedItem";
      links[1].className = "";
      links[2].className = "";

    });

    $('label[for=choice-2]').click(function () {
      const item = document.getElementById('ncard')
      item.value = "4P"
      const links = document.querySelectorAll(`section.background-select .card .color-choices div label`);
      links[0].className = "";
      links[1].className += " checkedItem";
      links[2].className = "";

    });

    $('label[for=choice-3]').click(function () {
      document.getElementById('ncard').value = "JP"
      const links = document.querySelectorAll(`section.background-select .card .color-choices div label`);
      links[0].className = "";
      links[1].className = "";
      links[2].className += " checkedItem";

    });
  })

  let quiz_content = '<p class="modalCampaingTitle" >' + quiz.poll.name + '</p>' +
    '<p class="modalPollTitle">' + quiz.poll.description + '</p>' +
    '<br><br>';
  quiz_content += '<div class="row">' +
    '<div class="col-1">' +
    '</div>' +
    '<div class="col-10">' +
    '<div class="form-group">';
  let blockbutton = null
  let questionMap = quiz.poll.question.map(item => {
    let response_id = null
    let points = 0
    quiz_content += '<p class="classCenterQuestion">' + item.text + '</p><div class="row d-flex justify-content-center">';
    if (item.question_type == 'SELECT') {
      let options = item.options.map(element => {
        let response_choice
        let x = quiz.responses.map(response => {
          if (response.question_id == item.id) {
            response_id = response.id
            points = response.points
            response_choice = response.data_answers
          }
        })

        quiz_content += '<label for="' + item.text + '">' + element.text + '</label>';
        if (element.value == response_choice) {
          quiz_content += '<div class="col-1"><input class="form-control mt-1" type="radio" id="' + item.text + '" name="question_' + item.id + '" value="' + element.value + '" checked></div>';
        } else {
          quiz_content += '<div class="col-1"><input class="form-control mt-1" type="radio" id="' + item.text + '" name="question_' + item.id + '" value="' + element.value + '"></div>';
        }

      })
    } else if (item.question_type == 'IMAGE' || item.question_type == 'VIDEO') {
      let response_file = quiz.responses.map(response => {
        if (response.question_id == item.id) {
          response_id = response.id
          points = response.points
          return '<img class="form-control" style="width: 75%; height: 75%;" src="/static/question_images' + response.data_answers.substr(6, 100) + '">'
        }
      })
      quiz_content += '<input class="form-control" type="file" name="question_' + item.id + '">' + response_file;
    } else if (item.question_type == 'DADOS') {
      let response_text
      quiz.responses.map(response => {
        if (response.question_id == item.id) {
          response_id = response.id
          points = response.points
          response_text = response.data_answers
        }
      })
      if (response_text == undefined) {
        response_text = '1'
      }
      quiz_content += '<div id="dado" onclick="lanzar()" style="height: 270px; width: 270px; margin-left: 210px;">' + response_text + '</div>' +
        '<input id="ndado" style="visibility:hidden" name="question_' + item.id + '" ></input>';

    } else if (item.question_type == 'CARDS') {
      let response_text = ''
      quiz.responses.map(response => {
        if (response.question_id == item.id) {
          response_id = response.id
          points = response.points
          response_text = response.data_answers
        }

      })
      if(response_text){
        quiz_content += '<section class="background-select" onload="f()">' +
        '<div class="card">' +
        '<div class="color-choices">' +
        '<div>';
        if(response_text == '2C'){
          quiz_content += '<input id="choice-1" checked="checked"  type="radio" src="" href=""/>' +
          '<label for="choice-1" style="margin-left:-20px;" src="" href=""></label>';
        }
        quiz_content += '</div>' +
        '<div>';
        if(response_text == '4P'){
          quiz_content += '<input id="choice-2" type="radio" style=""/>' +
          '<label for="choice-2" style="margin-left:10px;"></label>';
        }
        quiz_content += '</div>' +
        '<div>';
        if(response_text == 'JP'){
          quiz_content += '<input id="choice-3" " type="radio" />' +
          '<label for="choice-3" style="margin-right:-50px;"></label>';
        }
        quiz_content += '</div>' +
        '</div>' +
        '</div>' +
        '</section>'+
        '<input id="ncard" style="visibility:hidden" name="question_' + item.id + '" ></input>';
      }else{
        quiz_content += '<section class="background-select" onload="f()">' +
        '<div class="card">' +
        '<div class="color-choices">' +
        '<div>' +
        '<input id="choice-1" checked="checked"  type="radio" src="" href=""/>' +
        '<label for="choice-1" style="margin-left:-20px;" src="" href=""></label>' +
        '</div>' +
        '<div>' +
        '<input id="choice-2" type="radio" style=""/>' +
        '<label for="choice-2" style="margin-left:10px;"></label>' +
        '</div>' +
        '<div>' +
        '<input id="choice-3" " type="radio" />' +
        '<label for="choice-3" style="margin-right:-50px;"></label>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</section>'+
        '<input id="ncard" style="visibility:hidden" name="question_' + item.id + '" ></input>';
      }

    } else {
      let response_text
      quiz.responses.map(response => {
        if (response.question_id == item.id) {
          response_id = response.id
          points = response.points
          response_text = response.data_answers
        }
      })
      if (response_text == undefined) {
        response_text = ''
      }
      quiz_content += '<textarea class="form-control" id="' + item.text + '" name="question_' + item.id + '" rows="4" cols="50">' + response_text + '</textarea>';
    }
    if (quiz.evaluate == true) {
      if (parseInt(points) > 0) {
        blockbutton = blockbutton != false ? true : false
        quiz_content += '<br><label style="inline-size: -webkit-fill-available; text-align-last: center;" for="">Puntaje:' + points + ' </label>' +
          '<input id="points" style="visibility:hidden" value="' + points + '" name="question_' + item.id + '"></input>';
      } else {
        blockbutton = false
        quiz_content += '<br><label style="inline-size: -webkit-fill-available; text-align-last: center;" for="">Puntaje: </label>' +
          '<input id="points" name="question_' + item.id + '"></input>';

      }
      quiz_content += '<input id="response_id" value="' + response_id + '" style="visibility:hidden" name="question_' + item.id + '"></input>';

    }

    quiz_content += '</div><br><br>';
    points = null
    response_id = null
  })
  if ((!quiz.responses.length | quiz.evaluate == true) & (blockbutton == false | blockbutton == null)) {
    quiz_content += '<div class="modal-footer" style="justify-content: center;">' +
      '<button class="btn btn-primary">Enviar</button><button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>' +
      '</div>';
  }
  quiz_content += '</div>' +
    '<div class="col-1"></div>' +
    '</div>' +
    '</div>';
  return quiz_content

}



function formatDate(date) {
  var d = new Date(date),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2)
    month = '0' + month;
  if (day.length < 2)
    day = '0' + day;

  return [year, month, day].join('-');
}

function getCookie(name) {
  var cookieValue = null;
  if (document.cookie && document.cookie != '') {
    var cookies = document.cookie.split(';');
    for (var i = 0; i < cookies.length; i++) {
      var cookie = jQuery.trim(cookies[i]);
      // Does this cookie string begin with the name we want?
      if (cookie.substring(0, name.length + 1) == (name + '=')) {
        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
        break;
      }
    }
  }
  return cookieValue;
}


var csrftoken = getCookie('csrftoken');

