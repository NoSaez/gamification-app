from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.db import models
#from django.forms.models import _Labels

from AppCompany.models import Company
from AppPoll.models import Poll , PollCampaign , PollQuestion,  QuestionTextOption,PollQuestionChoiceOption
from django.forms import ModelForm, fields
from AppQuiz.models import QuizQuestionTextOption,QuizQuestionChoiceOption

from django.forms import ValidationError

# Create your forms here.

class NewUserForm(UserCreationForm):
  email = forms.EmailField(required=False)
  name= forms.CharField(min_length=1, max_length=50 , label='Nombre usuario' )

  class Meta:
      from django.contrib.auth import get_user_model
      User = get_user_model()
      model = User
      fields = ("company","name","email","role", "password1", "password2", "is_staff")
      labels = {
          'company': 'Nombre Compañia',
          'name': 'Nombre usuario ',
          'email': 'Correo electronico',
          'role': 'Rol Usuario',
          'password1': 'Constraseña',
          'password2': 'Repetir Constraseña',
          'is_staff': 'Es staff',
      }
      def save(self, commit=True):
        user = super(NewUserForm, self).save(commit=False)
        user.email = self.cleaned_data['email']
        if commit:
          user.save()
        return user




##New company form
class CompanyForm(ModelForm):
  #Validaciones 
  name= forms.CharField(min_length=1, max_length=50 , label='Nombre compañia')
  users_number=forms.IntegerField(min_value=1, max_value=500,label='Numero de usuarios')
  
  #que no se ingresen dato como espacio' '
  # def clean_name(self):
  #   name = self.cleaned_data['name']
  #   name = name.strip()
  #   if len(name) == 0:
  #       raise forms.ValidationError('Nombre en blanco')
  #   else:
  #     return name

#{'name': [u'Comment can not be blank']}

  class Meta:
    model=Company
    fields = [
      'name',
      'status',
      'users_number',
    ]
    labels = {
      
      'status': 'Estado',
      
    }
    widgets = {
      'name':forms.TextInput(attrs={'class':'form-control'}),
      'status':forms.Select(attrs={'class':'form-control'}),
    }



#Poll
class NewPollForm(forms.ModelForm):
   #Validaciones 
  name= forms.CharField(min_length=1,   max_length=250 , label='Nombre Encuesta')
  description= forms.CharField( min_length=1,  max_length=250 , label='Descripcion')
  
  # #Para que el nombre no se repita
  # def clean_name(self):
  #     names=self.cleaned_data["name"]
  #     existe=Poll.objects.filter(name__iexact=names).exists()
      
  #     if existe:
  #       raise ValidationError("Esta Encuesta ya existe")
  #     return names

  class Meta:
      model = Poll
      fields = [
        'name',
        'description',
      ]
      labels = {
        'name': 'Nombre Encuesta',
        'description': 'Descripcion',

      }
      widgets = {
        'name':forms.TextInput(attrs={'class':'form-control'}),
        'description':forms.Textarea(attrs={'class':'form-control'}),
      }
      def save(self, commit=True):
        poll = super(NewPollForm, self).save(commit=False)
        if commit:
          poll.save()
        return poll


#Polls Campaigns
class NewPollCampaignsForm(forms.ModelForm):
   #Validaciones 
  name= forms.CharField( min_length=1,  max_length=250 , label='Nombre campaña')  

  # #Para que el nombre no se repita
  # def clean_name(self):
  #     names=self.cleaned_data["name"]
  #     existe=PollCampaign.objects.filter(name__iexact=names).exists()
      
  #     if existe:
  #       raise ValidationError("Esta Campaña ya existe")
  #     return names

  class Meta:
    model = PollCampaign
    fields = [
        'quiz',
        'name',
        'status',
      ]
    labels = {
        'quiz':'Test',
        'name': 'Nombre campaña',
        'status': 'Estado',

      }
    widgets = {
        'quiz':forms.Select(attrs={'class':'form-control'}),
        'name':forms.TextInput(attrs={'class':'form-control'}),
        'status':forms.Select(attrs={'class':'form-control'}),
      }
    def save(self, commit=True):
        pollcam = super(NewPollCampaignsForm, self).save(commit=False)
        if commit:
          pollcam.save()
        return pollcam


#new_PollQuestions
class NewPollQuestionsForm(ModelForm):
  #Validaciones 
  name= forms.CharField( min_length=1,  max_length=250 , label='Nombre pregunta') 
  text= forms.CharField( min_length=1,  max_length=250 , label='Texto')
  help_text= forms.CharField( min_length=1,  max_length=250 , label='Texto de ayuda')


  # #Para que el nombre no se repita
  # def clean_name(self):
  #     names=self.cleaned_data["name"]
  #     existe=PollQuestion.objects.filter(name__iexact=names).exists()
      
  #     if existe:
  #       raise ValidationError("Esta Pregunta ya existe")
  #     return names


  class Meta:
    model = PollQuestion
    fields = [
        'name',
        'text',
        'help_text',
        'quiz',
        'question_type',
        'required',
      ]
    labels = {
      
        'name':'Nombre pregunta',
        'text': 'texto',
        'help_text': 'Texto de ayuda',
        'quiz': 'Encuesta',
        'question_type': 'tipo de pregunta',
        'required': 'requerido',
      }
  widgets = {
        'name':forms.TextInput(attrs={'class':'form-control'}),
        'text':forms.TextInput(attrs={'class':'form-control'}),
        'help_text':forms.TextInput(attrs={'class':'form-control'}),
        'question_type':forms.Select(attrs={'class':'form-control'}),

      }
  def save(self, commit=True):
          pollques = super(NewPollQuestionsForm, self).save(commit=False)
          if commit:
            pollques.save()
          return pollques


#####################3
#form questions text
 #Validaciones 
  value= forms.IntegerField(min_value=1, max_value=500,label='Valor')
  text= forms.CharField(   max_length=50 , label='Texto')

class FormQuestionsText(forms.ModelForm):
  class Meta:
    model = QuestionTextOption
    fields = [
        'question',
        'value',
        'text',

      ]
    labels = {
        'question': 'Pregunta',
        'value':'Valor',
        'text': 'Texto',


      }
    widgets = {
        'value':forms.TextInput(attrs={'class':'form-control'}),
        'text':forms.Textarea(attrs={'class':'form-control'}),
        'question':forms.Select(attrs={'class':'form-control'}),
      }
    def save(self, commit=True):
        text = super(FormQuestionsText, self).save(commit=False)
        if commit:
          text.save()
        return text


#form questions text
class FormQuestionsChoice(forms.ModelForm):

  value= forms.IntegerField(min_value=1, max_value=500,label='Valor')
  text= forms.CharField(   max_length=50 , label='Texto')

  class Meta:
    model = PollQuestionChoiceOption
    fields = [
        'question',
        'is_correct',
        'value',
        'text',
      ]
    labels = {
        'question':'Pregunta',
        'is_correct': 'es correcto',
        'value': 'Valor',
        'text': 'Texto',
      }
    widgets = {
        'question':forms.Select(attrs={'class':'form-control'}),
        'value':forms.TextInput(attrs={'class':'form-control'}),
        'text':forms.Textarea(attrs={'class':'form-control'}),

      }
    def save(self, commit=True):
        text = super(FormQuestionsText, self).save(commit=False)
        if commit:
          text.save()
        return text
      


#Form envio email.

class formemail(forms.Form):
    mail= forms.EmailField()
    messege=forms.CharField()
  






