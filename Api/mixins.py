from rest_framework import mixins
from rest_framework.response import Response
from rest_framework import status


class CreateModelMixin(mixins.CreateModelMixin):

    def perform_create(self, serializer):
        return serializer.save()

    def get_data_create(self):
        data = self.request.data
        return data

    def create(self, request, *args, **kwargs):

        # Get data from method get_data_create
        data = self.get_data_create()

        # Create object from data or raise errors
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        obj = self.perform_create(serializer)

        # update headers
        headers = self.get_success_headers(serializer.data)

        # get serializer depends if detail has other changes
        self.action = 'retrieve'
        serializer = self.get_serializer(obj)
        return Response(
            serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class UpdateModelMixin(mixins.UpdateModelMixin):

    def perform_update(self, serializer):
        return serializer.save()

    def get_data_update(self):
        data = self.request.data.copy()
        return data

    def update(self, request, *args, **kwargs):

        # Get data from method get_data_create
        data = self.get_data_update()

        # Update object from data or raise errors
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(
            instance, data=data, partial=partial)
        serializer.is_valid(raise_exception=True)
        obj = self.perform_update(serializer)

        # get serializer depends if detail has other changes
        self.action = 'retrieve'
        serializer = self.get_serializer(obj)

        if getattr(instance, '_prefetched_objects_cache', None):

            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)


class DestroyModelMixin(mixins.DestroyModelMixin):
    pass
