from django.urls import path, include
from rest_framework import routers
from rest_framework.authtoken.views import obtain_auth_token

from AppPoll.viewsets import MyPollsViewsets
# from clients.viewsets import ProspectClientViewSet
# from todo.viewsets import RequestViewSet
# from notifications.viewsets import UserNotificationViewSet

router = routers.DefaultRouter()
router.register(r'my-polls', MyPollsViewsets, 'my-polls')

urlpatterns = router.urls

urlpatterns = [
    path('', include(router.urls)),
]