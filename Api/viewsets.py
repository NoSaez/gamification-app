
from rest_framework import mixins, viewsets
from rest_framework.response import Response
from collections import OrderedDict

from .mixins import CreateModelMixin
from .mixins import UpdateModelMixin
from .mixins import DestroyModelMixin


class ReadOnlyModelViewSet(mixins.RetrieveModelMixin,
                           mixins.ListModelMixin,
                           viewsets.GenericViewSet):
    """
    A viewset that provides default `list()` and `retrieve()` actions.
    """
    model = None
    serializer_detail = None
    serializer_create = None
    serializer_update = None

    def get_queryset(self):
        return self.model.objects.all()

    def get_serializer(self, *args, **kwargs):
        """
        Return the serializer instance that should be used for validating and
        deserializing input, and for serializing output.
        """
        serializer_class = self.get_serializer_class()
        kwargs['context'] = self.get_serializer_context()
        return serializer_class(*args, **kwargs)

    def get_serializer_class(self):
        """
        Return the class to use for the serializer.
        Defaults to using `self.serializer_class`.
        You may want to override this if you need to provide different
        serializations depending on the incoming request.
        (Eg. admins get full serialization, others get basic serialization)
        """

        assert self.serializer_class is not None, (
            "'%s' should either include a `serializer_class` attribute, "
            "or override the `get_serializer_class()` method."
            % self.__class__.__name__
        )

        action = self.action

        serializer_action_classes = {
            'list': self.serializer_class,
            'retrieve': self.serializer_detail,
            'create': self.serializer_create,
            'update': self.serializer_update,
            'partial_update': self.serializer_update,
        }

        return serializer_action_classes.get(
            action) or self.serializer_class


class ModelViewSet(
        ReadOnlyModelViewSet,
        CreateModelMixin,
        UpdateModelMixin,
        DestroyModelMixin):

    """
    A viewset that provides default
    `list()`, `retrieve()`, `create()`,
    `update()` and `destroy()` actions.
    """
