from django.utils.translation import ugettext_lazy as _

class Type(object):
    ASSESSED = 'AS'
    ADMINISTRATOR = 'AD'
    CONSULTANT = 'CO'

    choices = (
        (ASSESSED, _('EVALUADO')),
        (ADMINISTRATOR, _('ADMINISTRADOR')),
        (CONSULTANT, _('EVALUADOR')),
    )
