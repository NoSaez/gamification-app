#django
from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.utils.translation import ugettext_lazy as _

#models
from AppBase.models import BaseModel
from AppCompany.models import Company
from AppUser.managers import UserManager

#enums
from AppBase.enums import Status
from .enums import Type


class User(AbstractBaseUser, PermissionsMixin, BaseModel):

    #ForeignKey
    company = models.ForeignKey(
        Company,
        on_delete=models.CASCADE,
        blank=True,
        null = True,
        help_text=_(u"User company")
    )

    # type = models.CharField(
    #     _('type'),
    #     max_length=255,
    #     choices=Type.choices,
    #     blank=True,
    #     default=Type.ASSESSED,
    # )
    name = models.CharField(
        _('name'),
        max_length=255,
        blank=True,
        null=True,
    )
    status = models.CharField(
        _('status'),
        max_length=255,
        choices=Status.choices,
        blank=True,
        default=Status.ENABLE,
    )

    email = models.EmailField(
        _('email address'),
        blank=True,
        unique=True,
        null=True,
    )

    role = models.CharField(
        _('status'),
        max_length=255,
        choices=Type.choices,
        blank=True,
        default=Type.ASSESSED,
    )

    is_staff = models.BooleanField(
        _('staff status'), default=False,
        help_text=_('Designates whether the user can log into this admin '
                    'site.'),
    )
    objects = UserManager()

    EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []


    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')