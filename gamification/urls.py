"""gamification URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from AppBase.forms import NewPollCampaignsForm
from AppBase.views import LoginView, DashboardView, AdminDashboardView,list_user,list_company,viewAdmin,PollList,NewPollView, \
        NewPollCampaignView , NewPollQuestionsView ,Game_dado,Game_card, editar_company,editar_user,editar_poll,PollListCampaing,PollListQuestions,\
        editar_campaing,editar_questions,  CreatePollView, PollAsignamentView, PollAsignamentListView, NewQuestionText,NewQuestionChoice,ListQuestionText,ListQuestionChoice,editar_choice,editar_text,\
        new_company   , send_email,viewprocexcompa,list_procesosxempresa,ver_respuestas
from django.contrib.auth.views import login_required
from django.conf.urls import url

from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', LoginView.as_view(), name='login'),
    path('accounts/', include('AppBase.urls')),
    path('home/', DashboardView.as_view(), name='home'),
    path('api/1.0/', include('Api.urls')),
    path('poll/', include('AppPoll.urls')),
    path('new_user/', AdminDashboardView.as_view(), name='new_user'),
    path('list_user/', list_user, name='list_user'),
    path('new_company/', new_company.as_view(), name='new_company'),
    path('list_company/', list_company, name='list_company'),
    path('administrator/', viewAdmin, name='administrator'),##ADMINNISTRATTOR
    path('list_poll/',PollList.as_view(), name='list_poll'),
    path('list_asignament/',PollAsignamentListView.as_view(), name='list_asignament'),

    #url(r'^list_poll/$', login_required(PollList.as_view()), name='list_poll'),
    path('new_poll/', NewPollView.as_view(), name='new_poll'),
    path('new_pollCampaign/', NewPollCampaignView.as_view(), name='new_pollCampaign'),
    path('list_pollQuestions/',PollListQuestions.as_view(), name='list_pollQuestions'),
    path('new_PollQuestions/', NewPollQuestionsView.as_view(), name='new_PollQuestions'),
    path('list_pollCampaing/',PollListCampaing.as_view(), name='list_pollCampaing'),
    ###
    #path('new_pollfull/', viewpoll, name='viewpoll'),
    path('new_pollFull/', CreatePollView.as_view(), name='new_pollFull'),
    path('new_asignament/', PollAsignamentView.as_view(), name='new_asignament'),


    #path('new_pollFull/', CreatePollView.as_view(), name='new_pollFull'),
    ##
    path('game_dado/', Game_dado.as_view(), name='viewpoll'),#dado
    path('game_carta/', Game_card.as_view(), name='viewpoll'),#carta
    ##
    path('editar_company/<id>/', editar_company, name='editar_company'),#editar compañia
    path('editar_user/<id>/', editar_user, name='editar_user'),#editar usuario
    path('editar_poll/<id>/', editar_poll, name='editar_poll'),#editar poll
    path('editar_campaing/<id>/', editar_campaing, name='editar_campaing'),#editar campaing
    path('editar_questions/<id>/', editar_questions, name='editar_questions'),#editar campaing
    ##
    path('newQuestionText/', NewQuestionText.as_view(), name='NewQuestionText'),#nueva pregunta tipo texto
    path('newQuestionChoice/', NewQuestionChoice.as_view(), name='NewQuestionChoice'),#nueva pregunta tipo texto
    path('listQuestionText/', ListQuestionText.as_view(), name='ListQuestionText'),#Listar preguntas tipo texto
    path('listQuestionChoice/', ListQuestionChoice.as_view(), name='ListQuestionChoice'),#Listar preguntas tipo texto
    path('editar_text/<id>/', editar_text, name='editar_text'),#editar campaing
    path('editar_choice/<id>/', editar_choice, name='editar_choice'),#editar campaing  |
    #
    path('sendEmail', send_email, name='sendEmail'),
    #
    path('list_procesos_empresas/', viewprocexcompa, name='list_procesos_empresas'),#listar empresas para ver procesos
    path('ver_procesos_empresas/<id>/', list_procesosxempresa, name='listproce'),#ver procesos por empresas
    path('ver_respuestas/<id>/', ver_respuestas, name='ver_respuestas'),#ver procesos por empresas




]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

# if settings.DEBUG:
#     static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
#     urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)